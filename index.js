function trainer(name, friends){
    this.name = name;
    this.friends = friends;
}

let myPokemonTrainer = {
	name: "Ash Ketchum",
	talk: function(){
		console.log("Result of talk method");
		console.log("Pikachu! I choose you!");
	},
	age: 10,
	friends: {
		hoen: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	pokemon : ["Pikachi", "Charizard", "Squirtle", "Bulbasaur"],	
}

console.log(myPokemonTrainer);
console.log("Result from dot notation: ");
console.log(myPokemonTrainer.name);
console.log("Result of square bracket notation: ");
console.log(myPokemonTrainer.pokemon);
myPokemonTrainer.talk();

function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = 2 * level;
  this.attack = level;
  
  this.tackle = function(target) {
    let damage = (this.health - this.attack);
    target.health -= damage;
    
    if (target.health > 0) {
      console.log(this.name + " tackled " + target.name);
      console.log(target.name + " health is now " + target.health + " remaining");
    } else {
      target.faint();
    }
  };
  
  this.faint = function() {
    console.log(this.name + " fainted.");
  };
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let meowtwo = new Pokemon("Meowtwo", 100);
console.log(meowtwo);

geodude.tackle(pikachu);
console.log(pikachu);

meowtwo.tackle(geodude);
console.log(geodude);